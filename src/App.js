import React from "react";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./App.css";
import Homepage from "./pages/Homepage";
import PokemonPage from "./pages/PokemonPage";
import Header from "./component/Header";

function App() {
  return (
    <Router>
      <Header />
      <Container>
        <Route exact path="/" component={Homepage}></Route>
        <Route path="/pokemon/:id" component={PokemonPage}></Route>
      </Container>
    </Router>
  );
}

export default App;
