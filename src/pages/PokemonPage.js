import React, { useEffect, useState } from "react";
import axios from "axios";
import Loader from "../component/Loader";
import { Card, Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

const PokemonPage = ({ match }) => {
  const id = match.params.id;
  const [pokemonDetail, setPokemonDetail] = useState();
  const [loading, setLoading] = useState(true);

  const getPokemon = async (id) => {
    const details = await getPokemonData(id);
    setPokemonDetail(details.data);
    setLoading(false);
  };

  const getPokemonData = async (id) => {
    const response = await axios.get(`https://pokeapi.co/api/v2/pokemon/${id}`);
    return response;
  };

  useEffect(() => {
    getPokemon(id);
  }, []);

  return (
    <>
      {loading ? (
        <Loader />
      ) : (
        <Row>
          <Col xs={12} sm={12} md={12} lg={12} xl={12}>
            <Card
              className="my-3 p-3 rounded text-center shadow mb-5 bg-white"
              style={{ border: "none" }}
            >
              <Link to={`/pokemon/${pokemonDetail.id}`}>
                <Card.Img
                  style={{ width: "15rem" }}
                  src={pokemonDetail.sprites.other.dream_world.front_default}
                  variant="top"
                />
              </Link>
              <Card.Body
                className={`${pokemonDetail.types[0].type.name} rounded text-white `}
              >
                <Link to={`/pokemon/${pokemonDetail.id}`} className="link-name">
                  <Card.Title as="div">
                    <strong>
                      {pokemonDetail.id}{" "}
                      {pokemonDetail.name.charAt(0).toUpperCase() +
                        pokemonDetail.name.slice(1)}
                    </strong>
                  </Card.Title>
                </Link>
              </Card.Body>
            </Card>
          </Col>
          <Col xs={12} sm={12} md={12} lg={12} xl={12}>
            <Card
              className="my-3 p-3 rounded text-center shadow mb-5 bg-white"
              style={{ border: "none" }}
            >
              <Card.Body>
                <Card.Text>
                  <Row>
                    {pokemonDetail.types.map((item) => (
                      <Col key={item.type.name}>
                        <div
                          className={`${item.type.name} rounded px-4 py-1`}
                          style={{ color: "white" }}
                        >
                          {item.type.name.toUpperCase()}
                        </div>
                      </Col>
                    ))}
                  </Row>
                  <Row>
                    <Col>
                      <Card.Img
                        style={{ width: "15rem" }}
                        src={
                          pokemonDetail.sprites.other.dream_world.front_default
                        }
                      />
                      <Card.Text>Normal Form</Card.Text>
                    </Col>
                    <Col>
                      <Card.Img
                        style={{ width: "15rem" }}
                        src={pokemonDetail.sprites.other.home.front_default}
                      />
                      <Card.Text>Shiny Form</Card.Text>
                    </Col>
                  </Row>

                  <Row className="mt-4">
                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                      <div
                        className="px-4 py-1 rounded"
                        style={{ border: "1px black solid" }}
                      >
                        Abilities
                      </div>
                    </Col>
                  </Row>

                  <Row className="text-center">
                    {pokemonDetail.abilities.map((item) => (
                      <Col
                        key={item.ability.name}
                        xs={6}
                        sm={6}
                        md={6}
                        lg={6}
                        xl={6}
                      >
                        <div className="rounded px-4 py-1">
                          {item.ability.name.toUpperCase()}
                        </div>
                      </Col>
                    ))}
                  </Row>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      )}
    </>
  );
};

export default PokemonPage;
