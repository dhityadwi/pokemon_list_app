import React from "react";
import { Col, Row, Spinner } from "react-bootstrap";

const Loader = () => {
  return (
    <div className="d-flex justify-content mt5" style={{ height: "100vh" }}>
      <Row>
        <Col>
          <Spinner
            className="spinner-border spinner-border-lg"
            role="status"
            style={{ height: "5vh", width: "5vh" }}
          ></Spinner>
        </Col>
      </Row>
      <Row>
        <Col>
          <div className="mx3"></div>Loading Pokemon List...
        </Col>
      </Row>
    </div>
  );
};

export default Loader;
