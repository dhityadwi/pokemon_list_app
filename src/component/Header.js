import React from "react";
import { Container, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <header>
      <Navbar bg="dark" variant="dark" expand="lg" collapseOnSelect>
        <Container>
          <Link to="/" className="link-nav">
            <Navbar.Brand>Home List Pokemont</Navbar.Brand>
          </Link>
        </Container>
      </Navbar>
    </header>
  );
};

export default Header;
